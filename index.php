<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        include_once ("vendor\autoload.php");
        
        use App\bitm\pencil;
        use App\bitm\house;
        use App\bitm\car;
        use App\bitm\book;
        
         $info_pencil=new pencil();
         $info_pencil->sayAbout();
         
         $info_house=new house();
         echo "<hr \>";
         $info_house->infoHouse();
         echo "<hr \>";
         $info_car=new car();
         $info_car->infoCar();
         echo "<hr \>";
         $info_book=new book();
         $info_book->infoBook();
         echo "<hr \>";
        
        ?>
    </body>
</html>
